package com.wages.modules.sys.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wages.modules.sys.user.entity.SysUser;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-09
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
