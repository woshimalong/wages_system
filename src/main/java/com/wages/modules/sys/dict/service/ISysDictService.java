package com.wages.modules.sys.dict.service;

import com.wages.modules.sys.dict.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
public interface ISysDictService extends IService<SysDict> {

}
