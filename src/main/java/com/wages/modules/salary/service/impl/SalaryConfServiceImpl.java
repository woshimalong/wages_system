package com.wages.modules.salary.service.impl;

import com.wages.modules.salary.entity.SalaryConf;
import com.wages.modules.salary.mapper.SalaryConfMapper;
import com.wages.modules.salary.service.ISalaryConfService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
@Service("salaryConfService")
@Transactional
public class SalaryConfServiceImpl extends ServiceImpl<SalaryConfMapper, SalaryConf> implements ISalaryConfService {

}
