package com.wages.modules.salary.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wages.modules.salary.entity.SalaryDetail;
import com.wages.modules.salary.mapper.SalaryDetailMapper;
import com.wages.modules.salary.service.ISalaryDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
@Service("salaryDetailService")
@Transactional
public class SalaryDetailServiceImpl extends ServiceImpl<SalaryDetailMapper, SalaryDetail> implements ISalaryDetailService {

}
