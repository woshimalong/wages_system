package com.wages.modules.salary.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wages.modules.salary.entity.WorkRecord;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
public interface IWorkRecordService extends IService<WorkRecord> {

}
