package com.wages.modules.sys.menu.mapper;

import com.wages.modules.sys.menu.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-07
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
