package com.wages.modules.sys.log.service;

import com.wages.modules.sys.log.entity.SysBizLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 业务日志 服务类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-10
 */
public interface ISysBizLogService extends IService<SysBizLog> {

}
