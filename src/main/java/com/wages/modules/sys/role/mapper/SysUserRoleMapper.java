package com.wages.modules.sys.role.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wages.modules.sys.role.entity.SysUserRole;

/**
 * <p>
 * 用户和角色表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-09
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
