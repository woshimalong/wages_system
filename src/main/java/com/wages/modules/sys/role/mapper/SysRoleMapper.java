package com.wages.modules.sys.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wages.modules.sys.role.entity.SysRole;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-08
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
