package com.wages.modules.sys.role.service;

import com.wages.modules.sys.role.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色表 服务类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-09
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
