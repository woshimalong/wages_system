package com.wages.modules.sys.dept.mapper;

import com.wages.modules.sys.dept.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-07
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
