package com.wages.modules.salary.service;

import com.wages.modules.salary.entity.SalaryDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
public interface ISalaryDetailService extends IService<SalaryDetail> {

}
