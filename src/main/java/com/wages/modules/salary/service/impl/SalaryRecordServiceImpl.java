package com.wages.modules.salary.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wages.modules.salary.entity.SalaryRecord;
import com.wages.modules.salary.mapper.SalaryRecordMapper;
import com.wages.modules.salary.service.ISalaryRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
@Service("salaryRecordService")
@Transactional
public class SalaryRecordServiceImpl extends ServiceImpl<SalaryRecordMapper, SalaryRecord> implements ISalaryRecordService {

}
