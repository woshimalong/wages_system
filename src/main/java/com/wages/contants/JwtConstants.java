package com.wages.contants;

/**
 * jwt配置类
 * @author 553039957@qq.com
 */
public interface JwtConstants {

    String AUTH_HEADER = "token";

    String SECRET = "defaultSecret";

    Long EXPIRATION = 604800L;

    String AUTH_PATH = "/api/login";
}
