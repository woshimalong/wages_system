package com.wages.modules.sys.dict.mapper;

import com.wages.modules.sys.dict.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
