package com.wages.modules.salary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wages.modules.salary.entity.SalaryCalc;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-14
 */
public interface SalaryCalcMapper extends BaseMapper<SalaryCalc> {

}
