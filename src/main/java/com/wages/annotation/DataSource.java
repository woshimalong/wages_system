package com.wages.annotation;


import java.lang.annotation.*;

/**
 * 多数据源切换注解
 *
 * @author 553039957@qq.com
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    /**
     * 数据源名称
     * @return
     */
    String db() default "";
}
