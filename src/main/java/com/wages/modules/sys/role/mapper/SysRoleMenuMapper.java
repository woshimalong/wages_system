package com.wages.modules.sys.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wages.modules.sys.role.entity.SysRoleMenu;

/**
 * <p>
 * 角色和菜单表 Mapper 接口
 * </p>
 *
 * @author 553039957@qq.com
 * @since 2019-05-09
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
